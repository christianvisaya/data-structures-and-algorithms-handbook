# Notes

In this section, we compile a collection of our notes and 'Aha! moments.' These are insights, observations, and key takeaways that we've gathered during our exploration and study of various topics. It serves as a reservoir of knowledge and valuable insights that we've encountered on our learning journey. Feel free to explore and expand your understanding with the information presented here.

- Arrays
- Linked Lists (Singly and Doubly)
- Stacks and Queues
- Hash Tables
- Trees
- Linear Search
- Binary Search
- Bubble Sort
- Selection Sort
- Insertion Sort
- Merge Sort
- Quick Sort
- FIFO
- FILO
- Constant runtime O(1)
- Logarithmic runtime O(log n)
- Linear runtime O(n)
- Quadratic runtime O(n^2)
- Quasilinear runtime O(n log n)
- Cubic runtime O(n^3)
- Factorial runtime O(n!)
- Exponential runtime O(x^n)
- Polynomial runtime O(n^x)
- Brute force technique
- Traveling salesman
- Recursion
- Interface
- Best, average, and worst-case scenarios
- We measure efficiency by its worst-case scenario
- Time complexity is how fast or slow a task is completed
- Space complexity is how many resources we need to complete a task
- The growth rate is when we plotted the inputs and see how the algorithm will perform
- Big O notation is the theoretical definition of the complexity of an algorithm as a function of size

### Installing Python

#### For Windows

1. Go to "Microsoft store"
2. Search for Python
3. Install Python

![alt install-python-windows.jpg](/images/install-python-windows.png)

#### For macOS

1. [Download this](https://www.python.org/ftp/python/3.10.7/python-3.10.7-macos11.pkg)
2. Follow installation guide

### Creating Remote Repository

# How to Create a Remote Repository in GitLab

GitLab is a web-based platform for managing Git repositories. Creating a remote repository in GitLab allows you to store your code and collaborate with others. Follow these steps to create a remote repository:

1. **Sign In or Sign Up:**
   - Go to [GitLab](https://gitlab.com/) in your web browser.
   - If you don't have an account, sign up for one. If you already have an account, sign in.
   - ![alt sign-in-using-google.png](../images/sign-in-using-google.png)

2. **Create a New Project:**
   - Once you are signed in, click on the blue "New Project" button located on the dashboard. GitLab will ask for a template, choose "Create blank project."
   - ![alt new-project-button.png](../images/new-project-button.png)
   - ![alt create-blank-project.png](../images/create-blank-project.png)

3. **Choose a Project Name:**
   - Enter "Data Structures and Algorithms" in the "Project name" field. This will be the name of your repository.

4. **Fill out Project Details:**
   - Fill out project details as indicated below.
   - ![alt fill-out-project-form.png](../images/fill-out-project-form.png)

5. **Create Project:**
   - Click the "Create project" button to create your remote repository.

6. **Access Your Repository:**
   - After creating the repository, you'll be redirected to the project's page. You can now access and manage your repository using the GitLab web interface.

7.  **Clone Your Repository:**
    - To work with the repository on your local machine, you'll need to clone it. Click the "Clone" button to get the repository URL and use Git commands like `git clone` to clone the repository to your computer.
    - ![alt clone-new-project.png](../images/clone-new-project.png)

You can now start pushing your code to this repository and add project members as needed.

### Code Reference
REPLIT link: https://replit.com/join/arboozduie-khuletutzmappet

### Basic Python
```
# Basic Python

# Define basic information
name = 'Chris'
age = 23
bank_balance = 0.1
blood_type = 'O'
birthday = 'December 25, 1999'
address = 'Larlin, Village'
favorite_cartoons = ['Dora', 'Peppa', 'Ben10']
todo_list = ['Clean', 'Laundry']
gender = ('Male', 'Female')

# Define family information using a dictionary
family = {
    'mother': 'Alicia',
    'father': {
        'age': 50,
        'mother': 'John'
    },
    'siblings': ['Jay', 'Thomas']
}


# Function to demonstrate variable scope and basic operations
def local():
  # Variable scope examples
  name = 'Chris'  # Public
  _name = 'Chris'  # Protected
  __name = 'Chris'  # Private

  # Print variables
  print(name)
  print(_name)
  print(__name)

  # Perform some operations
  _age = age + 1

  _bank_balance = bank_balance
  _bank_balance += 30000
  print('First: ', _bank_balance)

  _bank_balance = bank_balance
  _bank_balance = +30000  # Note: This line seems to have a typo, it should be _bank_balance += 30000
  print('Second: ', _bank_balance)

  # Print variable values
  print(age)
  print(_age)
  print('Bank balance:', bank_balance)
  print('_Bank balance:', _bank_balance)


# Call the local function
local()


# Function to check if age is old enough
def check_age(age):
  if age >= 18:
    print('Old enough')
  else:
    print('Too young')


# Example usage of check_age function
check_age(16)


# Function to check grades
def check_grades(grade):
  if grade >= 95:
    print('With honors')
    if grade == 98:
      print('Grade is 98')
  elif grade >= 90:
    print('Pass')
  elif grade >= 85:
    print('Semi pass')
  elif grade >= 80:
    print('Average')
  elif grade >= 75:
    print('Fail')
  else:
    print('Drop')


# Example usage of check_grades function
check_grades(70)
check_grades(85)
check_grades(90)
check_grades(95)
check_grades(98)
check_grades(100)


# Function to check if a book is a favorite cartoon
def check_books(book):
  if book not in favorite_cartoons:
    print('Book is a favorite')
  else:
    print('Book is not a favorite')


# Example usage of check_books function
check_books('Harry Potter')
check_books('Dora')

# String manipulation example
sentence = 'Data structure is not my favorite subject'


# Function to check if a word is in the sentence
def check_word(word):
  if word in sentence:
    print('Word is in the sentence')
  else:
    print('Word is not in the sentence')


# Example usage of check_word function
check_word('Data')
check_word('Algorithms')
```

### List and Dictionary
```
# Lists
print("=== Lists ===\n")
subjects = ['Math', 'English', 'PE']
print(subjects[0])  # Accessing elements by index
print(subjects[1])
print(subjects[2])
print(subjects)  # Printing the entire list
subjects.append('DSA')  # Adding an element to the end of the list
print(subjects)
subjects.pop()  # Removing the last element from the list
print(subjects)

# Tuple
print("=== Tuple ===\n")
devices = ('Phone', 'Desktop', 'Watch')
print(devices[0])
print(devices[1])
print(devices[2])

# Dictionary
print("=== Dictionary ===\n")
family = {
    'mother': 'Alicia',
    'father': {
        'age': 50,
        'name': 'John'
    },
    'siblings': ['Jay', 'Thomas']
}
print('Father age:', family['father']['age'])
print('Father name:', family['father']['name'])
family['grandmother'] = 'Lola'  # Adding a new key-value pair
family['grandfather'] = 'Lolo'
print(family['grandmother'])
print(family['grandfather'])
family['father'].pop('age')  # Removing a key-value pair
print(family)

# Looping through a list
print("=== Looping through a list ===\n")
for subject in subjects:
  print(subject)

# Looping using range
print("=== Looping using range ===\n")
for index in range(len(subjects)):
  print(index, subjects[index])

# Len function
print('=== Len function ===\n')
count = len(subjects)
print('Number of subjects:', count)

# Looping through a range of numbers
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print('=== Looping through a range of numbers ===\n')
for number in numbers:
  print(number)

print('=====')
for index in range(5, 10):  # Looping through a specific range
  print(numbers[index])

print('=====')
for index in range(0, 10, 2):  # Looping with a step
  print(numbers[index])

print('=====')

# While loop (commented out to avoid an infinite loop)
# counter = 0
# stop = 10
# while counter < stop:
#     print(counter)
#     if counter == 5:
#         continue
#     counter += 1

# Writing a function
print("=== Writing a function ===\n")


def greet(age, address, name='John'):
  print(f'Hello! {name} with an age of {age}, living in {address}')


greet(23, 'Apalit')

# Taking input from the user
number_list = []
for index in range(5):
  current_number = input(f'Choose a number [{index + 1}]: ')
  number_list.append(current_number)

# Function to find the maximum number in a list
print("=== Function to find the maximum number in a list ===\n")


def find_max(numbers_from_user):
  max_number = max(map(int, numbers_from_user))
  print(max_number)


find_max(number_list)
```

### List
```
# Create a list of your favorite fruits
fruits = ['Apple', 'Banana', 'Orange', 'Grapes', 'Watermelon']

# Print the entire list
print("List of Fruits:", fruits)

# Add a new fruit to the list
fruits.append('Mango')
print("After Adding Mango:", fruits)

# Remove a fruit from the list
fruits.remove('Banana')
print("After Removing Banana:", fruits)

# Check if a fruit is in the list
if 'Grapes' in fruits:
  print("Grapes are in the list!")

# Find the index of a specific fruit
index_of_orange = fruits.index('Orange')
print("Index of Orange:", index_of_orange)

# Change the value of a specific element
fruits[0] = 'Pineapple'
print("After Changing First Element to Pineapple:", fruits)
```

### 2D List
```
# Create a 2D list representing a 3x3 matrix
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# Print the entire matrix
print("Matrix:")
for row in matrix:
  print(row)

# Access a specific element in the matrix
element = matrix[1][2]
print("Element at Row 1, Column 2:", element)

# Modify a value in the matrix
matrix[0][0] = 99
print("After Changing Value at Row 0, Column 0 to 99:")
for row in matrix:
  print(row)
```

### 3D List
```
# Create a 3D list representing a 2x2x2 cube
cube = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]

# Print the entire 3D list
print("3D Cube:")
for layer in cube:
  for row in layer:
    print(row)

# Access a specific element in the cube
element = cube[1][0][1]
print("Element at Layer 1, Row 0, Column 1:", element)

# Modify a value in the cube
cube[0][1][0] = 99
print("After Changing Value at Layer 0, Row 1, Column 0 to 99:")
for layer in cube:
  for row in layer:
    print(row)
```

### Zoom
```
# Lists to keep track of users in the lobby and onboard
users_onboard = []
users_onlobby = []


# Function to add a user to the lobby
def join(user):
  users_onlobby.append(user)
  print(f"{user} joined the lobby.")


# Function to cancel a user's join request
def cancel_join():
  if users_onlobby:
    user_canceled = users_onlobby.pop()
    print(f"{user_canceled}'s join request has been canceled.")
  else:
    print("No users in the lobby to cancel.")


# Function to admit all users from the lobby to the onboard list
def admit_all():
  for user in users_onlobby:
    users_onboard.append(user)
  users_onlobby.clear()
  print("All users in the lobby have been admitted.")


# Function to admit a specific user from the lobby
def admit():
  if users_onlobby:
    print("Users in the lobby:")
    for iteration, user in enumerate(users_onlobby):
      print(f"user_id: {iteration}, user_name: {user}")

    user_id = input('Select user_id to admit: ')

    # Convert user input to an integer
    try:
      user_id = int(user_id)
    except ValueError:
      print("Invalid input. Please enter a valid user_id.")
      return

    if 0 <= user_id < len(users_onlobby):
      user = users_onlobby[user_id]
      users_onboard.append(user)
      users_onlobby.remove(user)
      print(f"{user} has been admitted.")
    else:
      print("Invalid user_id. Please select a valid user_id.")
  else:
    print("No users in the lobby to admit.")


# Function to kick a user from the onboard list
def kick(user):
  if user in users_onboard:
    users_onboard.remove(user)
    print(f"{user} has been kicked from the call.")
  else:
    print(f"{user} is not in the call.")


# Function to end the call and clear the onboard list
def end_call():
  users_onboard.clear()
  print("The call has ended.")


# Sample usage of the conferencing app
join('Chris')
join('Rafael')
join('MJ')
join('Jay')

print("\nUsers in the lobby:")
print(users_onlobby)
print("\nUsers onboard:")
print(users_onboard)

admit()

print("\nUsers in the lobby:")
print(users_onlobby)
print("\nUsers onboard:")
print(users_onboard)

# Example of kicking a user
kick('Chris')

print("\nUsers in the lobby:")
print(users_onlobby)
print("\nUsers onboard:")
print(users_onboard)

# Example of ending the call
end_call()

print("\nUsers onboard after ending the call:")
print(users_onboard)
```
