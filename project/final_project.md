# Project Instructions

## Objectives:

- Develop a simple terminal-based Bible app with basic functionalities.
- Apply appropriate data structures and algorithms in the app development.

## Scope:

### String Searching Algorithms:

Use efficient algorithms like Knuth-Morris-Pratt (KMP) or Boyer-Moore for Bible text searching. These algorithms enhance search performance.

### Hierarchical Data Structure:

Represent the Bible text hierarchically, using a tree or trie. Organize as books > chapters > verses for efficient navigation.

### Verse Bookmarking:

Utilize a hash table or balanced binary search tree to efficiently store bookmarked verses for quick retrieval.

### Verse of the Day:

Implement an algorithm to randomly select a daily verse using a hash table or array for quick retrieval.

### Search History:

Maintain a queue or linked list for storing search history, allowing users to easily review past searches.

## Limitation:

- Simplified User Interface: Basic command-line interface without advanced formatting.
- Limited Search Scope: Restrict search to exact matches or basic keyword searches within verses.
- Basic Bookmarking Features: Implement straightforward verse bookmarking without additional features.
- Minimal Error Handling: Focus on core functionality over extensive input validation.

## Results:

- Fully functional terminal-based Bible app on Replit.
- Provide user instructions in markdown format with clear information on basic commands and features. Include examples for illustration.
- Reflect on the project in markdown, highlighting lessons learned, areas of growth, and alignment with data structures and algorithms concepts.

## Timeline:

- Last week of January 2024

## Links:

- https://replit.com/team/lvcc-dsa/Final-Project-Terminal-Based-Bible-App
