# CodeSignal

**https://app.codesignal.com/arcade/intro**

![alt codesignal-arcade-1.png](../images/codesignal-arcade-1.png)

Let's dive into the world of coding awesomeness with CodeSignal. It's not just another platform—it's your secret weapon to become a coding ninja. We're talking about algorithms and data structures, the bread and butter of real-deal programming.

CodeSignal's got this killer collection of challenges that'll make your brain cells do the cha-cha. Think of it as your personal training ground, where you're not just learning theory but pulling off coding stunts like a pro. These challenges are like real-life puzzles, the kind that pops up in job interviews and makes you the go-to coder in your squad.

So, buckle up and get ready to crack these coding enigmas.

### Project Details

- Each week, students are to complete 1 chapter (or level).
- We have 8 weeks (2 months) to complete the following chapters:
    - The Journey Begins **(October 7)**
    - Edge of the Ocean **(October 21)**
    - Smooth Sailing **(TBA)**
    - Exploring the Waters **(TBA)**
    - Island of Knowledge **(TBA)**
- Students need to screenshot each result.
    - The screenshot should include “time to solve”; this will serve as an indicator that you’ve finished the challenge.
    - “Time to solve” will be used in ranking.

### Instructions

1. Go to **https://app.codesignal.com**, sign up using your Google account.
2. Once signed up, click the logo on the upper left, choose arcade.
   - ![alt codesignal-main.png](../images/codesignal-main.png)
3. Click the "Intro" card.
   - ![alt codesignal-intro.png](../images/codesignal-intro.png)
4. Solve challenges in sequence; challenges will be unlocked once you've solved the previous one.
   - ![alt codesignal-editor.png](../images/codesignal-editor.png)
5. Save your "Result" screen [here](https://drive.google.com/drive/folders/1Dx-wDFkkv5WgGF45q23tgLeN-LbBD-ve?usp=drive_link). Create your folder with the format: **lastname_firstname**.
6. Name your screenshot after the challenge; for example: **adjacentElementsProduct**.
7. The result screen should look something like this:
   - ![alt codesignal-result.png](../images/codesignal-result.png)